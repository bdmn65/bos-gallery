<?php 
$mysqli;
$messages = "";
$basePath = "images/gallery";


function addMessage( $message ) {
	global $messages;
	$messages .= "<span> $message </span>";
}

function hasMessages() {
	global $messages;
	return $messages != "";
}

function getMessages() {
	global $messages;
	return $messages;
}
function connect() {
	global $mysqli;
        require 'config/info.php';

	//echo "connecting ... ";
	if( isset( $mysqli ) ) {
		return;
	} 

	$mysqli = new mysqli("localhost", $user, $pwd, $db);
	
	$mysqli->query("SET NAMES 'utf8'");
	if ($mysqli->connect_errno) {
    		echo "MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		return null;
	} 
	//echo $mysqli->host_info . "<br>\n";
	return $mysqli;
}

function asArray( $result ) {
	$rows = array();
	while($row = $result->fetch_array()) {
		$rows[] = $row;
	}
	return $rows;	
}

function getCount( $table ) {
	global $mysqli;
	connect();
	$result = $mysqli->query("SELECT COUNT(*) AS c FROM `$table` " );
	$row = $result->fetch_assoc();
	return $row['c']; 
}

function getTopics() {
	global $mysqli;
	connect();
	$sql = "SELECT Gallery FROM `Pictures` GROUP BY Gallery";
	return asArray( $mysqli->query($sql) ); 
}

function getCountPictures( ) {
	return getCount( "Pictures");
}
 

function getCountVotes( ) {
	global $mysqli;
	connect();
	$sql = "SELECT SUM( Votes ) AS sum FROM Pictures";
	$result = $mysqli->query( $sql );
	$row = $result->fetch_assoc();
	return $row['sum']; 
}
 
function getPicturesForRating( $rating ) {
	global $mysqli;
	connect();
	$sql = "SELECT * FROM `Pictures` WHERE Votes > 0 AND 2*abs( Rating / Votes - $rating ) < 1";
	//echo $sql;
	return asArray( $mysqli->query($sql) ); 
}


function getPicturesForTopic( $topic ) {
	global $mysqli;
	connect();
	$sql = "SELECT * FROM `Pictures` WHERE `Gallery`='$topic' ORDER BY `Filename`";
	//echo $sql;
	return asArray( $mysqli->query($sql) ); 
}

function updateVote( $pid, $rating ) {
	global $mysqli;
	connect();
	$sql = "UPDATE Pictures SET Rating = Rating+$rating WHERE PID=$pid";
	$mysqli->query($sql);
	$sql =  "UPDATE Pictures SET Votes = Votes+1 WHERE PID =$pid";
	$mysqli->query($sql);
	$sql = "SELECT * FROM `Pictures` WHERE PID =$pid";
	return asArray( $mysqli->query($sql) );
}

function newComment( $pid, $comment ) {
	global $mysqli;
	connect();
	$sql = "UPDATE Pictures SET `Comment`='$comment' WHERE PID=$pid";
	$mysqli->query($sql);
	$sql = "SELECT * FROM `Pictures` WHERE PID =$pid";
	return asArray( $mysqli->query($sql) );
}

function hasFile( $path, $filename ) {
	global $mysqli;
	connect();
	$sql = "SELECT COUNT(*) AS c FROM `Pictures` WHERE `Path`='$path' AND `Filename`='$filename'";
	//echo $sql;
	$result = $mysqli->query( $sql );
	$row = $result->fetch_assoc();
	return $row['c'] > 0; 
}
function addFile( $fullpath ) {
	global $mysqli;
	connect();
	$parts = preg_split('#/#', $fullpath );
	$sem      = $parts[0];
	$group    = $parts[1];
	$topic    = $parts[2];
        $filename = $parts[3];
        $path     = "$sem/$group/$topic";
	if( hasFile( $path, $filename ) ) {
		return "exists already";
	}

        $sql = "INSERT INTO `Pictures` ( `Path`, `Filename`, `THMModule`, `Class`, `Gallery` ) VALUES ( '$path', '$filename', '$group', '$sem', '$topic' )";
	// echo $sql;
	if( ! $mysqli->query($sql) ) {
		return "Error";
	} else {
		return buildThumb( $path, $filename );
	}
}

function buildThumb(  $path, $filename ) {
	global $basePath;
	$fullname = "$basePath/$path/$filename";
	if(! ($original=imagecreatefromstring( file_get_contents($fullname) )) ) {
		return "$fullname ist kein Bild.";
	}
	$bild=ImageCreateTrueColor(128, 128);
	$imagesize = getimagesize($fullname); 
	$thumbname = preg_replace( '/\.(jpe?g|png)$/i', "-thumb.png", $filename);
	imagecopyresized ($bild , $original , 0 , 0 , 0 , 0 , 128, 128 , $imagesize[0] , $imagesize[1]);
	imagejpeg($bild, "$basePath/$path/$thumbname"); 

	return $thumbname;
}
function isLoggedIn() {
	//return $_SESSION["loggedin"] || $_SERVER['SERVER_NAME'] === 'localhost';
	return $_SESSION["loggedin"];
}

function loginUser( $name, $password ) {
	global $mysqli;
	connect();
	$sql = "SELECT COUNT(*) AS c  FROM nutzer WHERE `name`='$name' AND `pass`='$password'";
	//echo $sql;
	$result = $mysqli->query( $sql );
	$row = $result->fetch_assoc();
	return $row['c'] > 0;
}


?>
