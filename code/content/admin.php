<?php

$path    = "images/gallery/";
$sems    = scandir($path);
$neue    = 0;

foreach( $sems as $sem ) {
	if (strncmp($sem, ".", 1) === 0) continue;

	$groups = scandir($path . "/" . $sem );
	foreach( $groups as $group ) {
		if (strncmp($group, ".", 1) === 0) continue;

		$topics = scandir($path . "/" . $sem  . "/" . $group );
		foreach( $topics as $topic ) {
			if (strncmp($topic, ".", 1) === 0) continue;
			echo "$sem $group $topic<br>";
			
			$files = scandir($path . "/" . $sem  . "/" . $group . "/" . $topic );
			foreach( $files as $file ) {
				if (strncmp($file, ".", 1) === 0) continue;
				if (  strpos($file, '-thumb.') ) continue;
				$fullpath = "$sem/$group/$topic";
				if( ! hasFile($fullpath, $file) ) {
					echo " neu: <span class='new-label'>$fullpath/$file</span> <br>";
					++$neue;
				}
			}
		}
	}
}

if( $neue > 0 ) {
	echo '<button type="button" class="btn" id="abutton">Neue Aufnehmen</button>';
} else {
	echo 'alles aktuell, keine neuen Bilder vorhanden';
}
?>


<div id="messages"></div>

<script>

$(document).ready(function() {
    
    $('#abutton').click(function( ) {
	$( ".new-label" ).each(function( ) {
		var file = $( this ).text()
		console.log( file )
		$.get("api.php", {"action":"addFile", "file":file  }, function(data){
 			$("#messages").append( file  + ":  " + data + " <br>" );
		});
  	});

    });
});
</script>
