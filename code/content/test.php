<!-- Include the Polyfill -->
<script src="https://www.javapoly.com/javapoly.js"></script>

<!-- Write your Java code -->
<script type="text/java">
  package com.demo;
  import com.javapoly.dom.Window;

  public class Greeter
  {
    public static void sayHello(String name)
    {
      Window.alert("Hello " + name + ", from Java!");
    }
  }
</script>

<script type="text/javascript">
  function test() {
	com.demo.Greeter.sayHello("world");
  }
</script>

<div class="row"> 
<div class="col-md-6 col-md-offset-1">
<textarea id="usercode" style="background: #000000; color: #00FF00; font-family: monospace; text-decoration: none; width: 100%; height: 200px; border: none; padding: 10px; outline: none;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
import com.javapoly.dom.Window;

public class HelloWorld
{
  public static void main(String[] args)
  {
    Window.alert("Hello World, from Java!");
  }
}

</textarea>
<div style="width: 100%; background: #333333; color: #00FF00; padding: 10px; border-top: 2px solid #555555;">
        <button onclick="JavaPoly.type('com.javapoly.demo.HomepageDemo').then(function(HomepageDemo){HomepageDemo.compileAndRun(document.getElementById('usercode').value);});">Compile &amp; Run!</button></div>
<div style="width: 100%; background: #333333; color: #00FF00; padding: 10px; border-top: 2px solid #555555;">
        <button onclick="test()">Test!</button></div>

</div>
</div>