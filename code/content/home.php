<div class="container-fluid">  
<h2>Themen </h2>
<?php
$topics  = getTopics();
echo '<div class="row">';
foreach( $topics as $topic ) {
	$pictures  = getPicturesForTopic( $topic["Gallery"] );
	$picture   = $pictures[array_rand($pictures)];
	echo '<div class="col-sm-3">';
	echo '<a href="index.php?inhalt=show&topic=' . $topic["Gallery"]  . '">';
	echo "<img src='images/gallery/$picture[Path]/$picture[Filename]' class='topicImage' >";
	echo  '<div class="caption  text-center">' . $topic["Gallery"] . ' (' . count($pictures) . ')</div>';
	echo '</div>';
} 
echo '</div>';

?>


</div>